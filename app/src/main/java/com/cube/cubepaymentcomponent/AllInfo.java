package com.cube.cubepaymentcomponent;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllInfo {

    @SerializedName("all_info")
    @Expose
    private String allInfo;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;

    public String getAllInfo() {
        return allInfo;
    }

    public void setAllInfo(String allInfo) {
        this.allInfo = allInfo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}
