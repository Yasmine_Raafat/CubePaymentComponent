package com.cube.cubepaymentcomponent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //4 111 111 111 111 111
    Button pay;

    String mAuthorization;
    String mNonce;

//    String BASE_URL = "http://41.176.242.110:8080/api/v1/";
//    https://obtrackit.herokuapp.com/api/v1/docs/

    String BASE_URL = "https://obtrackit.herokuapp.com/api/v1/";
    String CLIENT_TOKEN_URL = "payment/client_token/";
    String CHECKOUT_URL = "payment/checkout/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pay = (Button) findViewById(R.id.btn_pay);
        pay.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pay:
                getToken();
                break;
        }
    }
    private void getToken() {

        Toast.makeText(getApplicationContext(), BASE_URL + CLIENT_TOKEN_URL, Toast.LENGTH_LONG).show();


        final GsonRequest<TokenModel> tokenModelGsonRequest = new GsonRequest<>(Request.Method.GET, BASE_URL + CLIENT_TOKEN_URL, TokenModel.class, null, null, new Response.Listener<TokenModel>() {
            @Override
            public void onResponse(TokenModel response) {
                if (null == response) {
                    //  Show error message to user.
                    return;
                }
                // receiver response and continue flow.


                mAuthorization = response.getToken();

                onBraintreeSubmit();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Show error message to user.
                handleVolleyError(error, getApplicationContext());
            }
        });

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(tokenModelGsonRequest);
    }

    public void onBraintreeSubmit() {
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(mAuthorization);
        startActivityForResult(dropInRequest.getIntent(this), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server

                mNonce = result.getPaymentMethodNonce().getNonce();

                Log.d("nooooooooooonce",mNonce);
                submitNonce();

                Toast.makeText(this, result.getPaymentMethodNonce().getNonce() + "", Toast.LENGTH_LONG).show();

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
            } else {
                // handle errors here, an exception may be available in
                String error = data.getSerializableExtra(DropInActivity.EXTRA_ERROR).toString();
                Toast.makeText(this,"DropInResult "+  error, Toast.LENGTH_LONG).show();


            }
        }
    }

    private void submitNonce() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("payment_method_nonce", mNonce);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final AllInfo[] allInfo = {new AllInfo()};

        final String mRequestBody = jsonObject.toString();
        final GsonRequest<AllInfo> allInfoGsonRequest = new GsonRequest<>(Request.Method.POST, BASE_URL + CHECKOUT_URL, AllInfo.class, null, mRequestBody, new Response.Listener<AllInfo>() {
            @Override
            public void onResponse(AllInfo response) {
                if (null == response) {
                    //  Show error message to user.
                    return;
                }
                // receiver response and continue flow.
                allInfo[0] = response;
                Toast.makeText(getApplicationContext(),allInfo[0].getAllInfo(),Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),allInfo[0].getTransactionId(),Toast.LENGTH_SHORT).show();



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  Show error message to user.
               handleVolleyError(error, getApplicationContext());
            }
        });

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(allInfoGsonRequest);
    }
    public static void handleVolleyError(VolleyError error, Context applicationContext) {

        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            Toast.makeText(applicationContext,"error_network_timeout",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(applicationContext,"error_authentication",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(applicationContext,"error_server",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(applicationContext,"error_network",
                    Toast.LENGTH_LONG).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(applicationContext,"error_parse",
                    Toast.LENGTH_LONG).show();
        }

    }

}
